package blockbuster.movierental;

import org.junit.*;

import static org.junit.Assert.assertTrue;


public class CustomerTest {

    private final RentalFactory rentalFactory = new RentalFactory();


    private static final String TUPETACO_NAME = "Tupetaco";
    private final Customer customer = new Customer("Tupetaco");

    @Test
    public void should_increase_amount_regular_movie_less_three_days_rented() throws Exception {
        Movie regularMovie = new Movie("regular", Movie.REGULAR);
        int lessThan3Days = 2;
        Rental regularRental = this.rentalFactory.getRental(regularMovie, lessThan3Days);

        this.customer.addRental(regularRental);
        String result = getResult(regularMovie, "1", 2, 2);

        assertTrue(result.equals(this.customer.statement()));
    }

    @Test
    public void should_increase_amount_regular_movie_more_three_days_rented() throws Exception {
        Movie regularMovie = new Movie("regular", Movie.REGULAR);
        Rental regularRental = this.rentalFactory.getRental(regularMovie, 4);
        this.customer.addRental(regularRental);
        String result = getResult(regularMovie, "1", 5, 5);
        assertTrue(result.equals(this.customer.statement()));
    }


    @Test
    public void should_increase_amount_children_less_four_days() throws Exception {
        Movie childrenMovie = new Movie("children", Movie.CHILDREN);
        Rental childrenRental = this.rentalFactory.getRental(childrenMovie, 2);
        this.customer.addRental(childrenRental);
        String result = getResult(childrenMovie, "1", 1.5, 1.5);
        assertTrue(result.equals(this.customer.statement()));
    }

    @Test
    public void should_increase_amount_children_more_four_days() throws Exception {
        Movie childrenMovie = new Movie("children", Movie.CHILDREN);
        Rental childrenRental = this.rentalFactory.getRental(childrenMovie, 4);
        this.customer.addRental(childrenRental);
        String result = getResult(childrenMovie, "1", 3, 3);
        assertTrue(result.equals(this.customer.statement()));
    }

    @Test
    public void should_increase_amount_new_release() throws Exception {
        Movie newReleaseMovie = new Movie("newRelease", Movie.NEW_RELEASE);
        Rental newReleaseRental = this.rentalFactory.getRental(newReleaseMovie, 1);
        this.customer.addRental(newReleaseRental);
        String result = getResult(newReleaseMovie, "1", 3, 3);
        assertTrue(result.equals(this.customer.statement()));
    }

    @Test
    public void should_increase_frequent_points_new_release() throws Exception {
        Movie newReleaseMovie = new Movie("newRelease", Movie.NEW_RELEASE);
        Rental newReleaseRental = this.rentalFactory.getRental(newReleaseMovie, 4);
        this.customer.addRental(newReleaseRental);
        String result = getResult(newReleaseMovie, "2", 12, 12);
        assertTrue(result.equals(this.customer.statement()));
    }

    @Test
    public void should_do_nothing_when_empty() throws Exception {
        String result = getResultWhenZero();
        assertTrue(result.equals(this.customer.statement()));
    }

    private String getResultWhenZero() {
        String result = "Rental Record for " + CustomerTest.TUPETACO_NAME + "\n";
        result += "You owed " + "0.0" + "\n";
        result += "You earned " + "0" + " frequent renter points\n";
        return result;
    }


    private String getResult(Movie movie, String points, double amount, double totalAmount) {
        String result = "Rental Record for " + CustomerTest.TUPETACO_NAME + "\n";
        result += "\t" + movie.getTitle() + "\t" + amount + "\n";
        result += "You owed " + totalAmount + "\n";
        result += "You earned " + points + " frequent renter points\n";
        return result;
    }
}
