package blockbuster.movierental;


class Customer {
    private final String name;
    private final Rentals rentals = new Rentals();

    Customer(String name) {
        this.name = name;
    }

    void addRental(Rental rental) {
        this.rentals.addRental(rental);
    }

    String statement() {
        StatementProcessor statementProcessor = new StatementProcessor();
        return statementProcessor.build(this.name, this.rentals);
    }

}
