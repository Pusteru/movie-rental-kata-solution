package blockbuster.movierental;

class StatementProcessor {

    String build(String userName, Rentals rentals) {
        return getHeader(userName) + getRentalsStatements(rentals) + getOweStatement(rentals) + getFrequencyPointsStatement(rentals);
    }

    private String getRentalsStatements(Rentals rentals) {
        StringBuilder statement = new StringBuilder();
        for (Rental rental : rentals.getRentals()) {
            statement.append("\t").append(rental.getMovieTitle()).append("\t").append(String.valueOf(rental.getMoneyCost())).append("\n");
        }
        return statement.toString();
    }

    private String getFrequencyPointsStatement(Rentals rentals) {
        return "You earned " + String.valueOf(rentals.getFrequencyPoints()) + " frequent renter points\n";
    }

    private String getOweStatement(Rentals rentals) {
        return "You owed " + String.valueOf(rentals.getTotalAmount()) + "\n";
    }

    private String getHeader(String userName) {
        return "Rental Record for " + userName + "\n";
    }

}
