package blockbuster.movierental;

abstract class Rental {

    private final Movie movie;
    final int daysRented;

    Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    String getMovieTitle() {
        return this.movie.getTitle();
    }

    abstract int getFrequentPoints();

    abstract double getMoneyCost();
}
