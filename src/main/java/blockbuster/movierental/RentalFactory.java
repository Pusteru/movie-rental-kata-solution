package blockbuster.movierental;

class RentalFactory {

    Rental getRental(Movie movie, int daysRented) {
        switch (movie.getPriceCode()) {
            case Movie.REGULAR:
                return new RegularRental(movie, daysRented);
            case Movie.NEW_RELEASE:
                return new NewReleaseMovieRental(movie, daysRented);
            case Movie.CHILDREN:
                return new ChildMovieRental(movie, daysRented);
            default:
                break;
        }
        return null;
    }
}
