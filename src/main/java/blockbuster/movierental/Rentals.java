package blockbuster.movierental;


import java.util.ArrayList;
import java.util.List;

class Rentals {
    private double totalAmount;
    private int frequencyPoints;
    private final List<Rental> rentals;

    Rentals() {
        this.totalAmount = 0;
        this.frequencyPoints = 0;
        this.rentals = new ArrayList<Rental>();
    }

    void addRental(Rental rental) {
        this.totalAmount += rental.getMoneyCost();
        this.frequencyPoints += rental.getFrequentPoints();
        this.rentals.add(rental);
    }

    double getTotalAmount() {
        return this.totalAmount;
    }

    int getFrequencyPoints() {
        return this.frequencyPoints;
    }

    List<Rental> getRentals() {
        return this.rentals;
    }

}
