package blockbuster.movierental;

class RegularRental extends Rental {

    RegularRental(Movie movie, int daysRented) {
        super(movie, daysRented);
    }

    @Override
    int getFrequentPoints() {
        return 1;
    }

    @Override
    double getMoneyCost() {
        return 2 + Math.max(0, this.daysRented - 2) * 1.5;
    }
}
