package blockbuster.movierental;

class NewReleaseMovieRental extends Rental {

    NewReleaseMovieRental(Movie movie, int daysRented) {
        super(movie, daysRented);
    }

    @Override
    int getFrequentPoints() {
        return this.daysRented > 1 ? 2 : 1;
    }

    @Override
    double getMoneyCost() {
        return this.daysRented * 3;
    }
}
