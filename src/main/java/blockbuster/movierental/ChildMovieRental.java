package blockbuster.movierental;

class ChildMovieRental extends Rental {

    ChildMovieRental(Movie movie, int daysRented) {
        super(movie, daysRented);
    }

    @Override
    int getFrequentPoints() {
        return 1;
    }

    @Override
    double getMoneyCost() {
        return 1.5 + Math.max(0, this.daysRented - 3) * 1.5;
    }
}
